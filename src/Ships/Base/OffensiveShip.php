<?php


namespace App\Base\Ships;

/**
 * Class OffensiveShip
 * @package App\Base\Ships
 */
abstract class OffensiveShip extends Ship
{

    protected bool $shieldsRaised = false;

    protected int $guns = 0;

    /**
     * Raises the ship's shields
     */
    public function raiseShields(): void
    {
        $this->shieldsRaised = true;
    }

    /**
     * Instructs the ship to fire it's available guns
     * This operation will uncover shields
     */
    public function attack(): void
    {
        $this->shieldsRaised = false;

        for ($i = 0; $i <= $guns; $i++) {
            echo "Firing guns: {$i}";
        }
    }
}