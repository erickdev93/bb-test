<?php

namespace App\Base\Ships;

/**
 * Class Ship
 * @package App\Base\Ships
 */
abstract class Ship
{

    protected int $x;

    protected int $y;

    /**
     * Moves the ship to given coordinates
     * @param int $x
     * @param int $y
     */
    public function move(int $x, int $y): void
    {
        $this->x = $x;
        $this->y = $y;
    }
}