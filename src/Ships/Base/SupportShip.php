<?php


namespace App\Base\Ships;

/**
 * Class SupportShip
 * @package App\Base\Ships
 */
abstract class SupportShip extends Ship
{

    protected MedicalUnit $medicalUnit;

    /**
     * SupportShip constructor.
     * @param MedicalUnit $medicalUnit
     */
    public function __construct(MedicalUnit $medicalUnit)
    {
        $this->medicalUnit = $medicalUnit;
    }


    /**
     * @return MedicalUnit
     */
    public function getMedicalUnit(): MedicalUnit
    {
        return $this->medicalUnit;
    }

    /**
     * @param MedicalUnit $medicalUnit
     */
    public function setMedicalUnit(MedicalUnit $medicalUnit): void
    {
        $this->medicalUnit = $medicalUnit;
    }

    abstract function executeSupport();
}