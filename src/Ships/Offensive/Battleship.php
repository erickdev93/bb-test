<?php

namespace App\Ships\Offensive;

use App\Base\Ships\OffensiveShip;

/**
 * Class Battleship
 * @package App\Ships\Offensive
 */
class Battleship extends OffensiveShip
{

    private bool $isCommander = false;

    /**
     * @return bool
     */
    public function isCommander(): bool
    {
        return $this->isCommander;
    }

    /**
     * @param bool $isCommander
     */
    public function setIsCommander(bool $isCommander): void
    {
        $this->isCommander = $isCommander;
    }
}