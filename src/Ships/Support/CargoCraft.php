<?php

namespace App\Ships\Support;

use App\Base\Ships\SupportShip;

/**
 * Class CargoCraft
 * @package App\Ships\Support
 */
class CargoCraft extends SupportShip
{

    public function executeSupport()
    {
        echo 'Executing cargo operation...';
    }
}