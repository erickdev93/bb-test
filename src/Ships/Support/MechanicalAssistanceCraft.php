<?php

namespace App\Ships\Support;

use App\Base\Ships\SupportShip;

/**
 * Class MechanicalAssistanceCraft
 * @package App\Ships\Support
 */
class MechanicalAssistanceCraft extends SupportShip
{

    public function executeSupport()
    {
        echo 'Giving mechanical support...';
    }
}