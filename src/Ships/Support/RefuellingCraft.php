<?php

namespace App\Ships\Support;

use App\Base\Ships\SupportShip;

/**
 * Class RefuellingCraft
 * @package App\Ships\Support
 */
class RefuellingCraft extends SupportShip
{

    function executeSupport()
    {
        echo 'Refuelling....';
    }
}